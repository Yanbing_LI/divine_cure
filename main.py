import pandas as pd
import os
import numpy as np
from unidecode import unidecode
import re
import datetime as dt
from datetime import timedelta

from src.load_clean import load_clean
import src.kpi_functions as function
import src.get_config as config_function


# Display max 10 rows of your data set
pd.set_option('display.max_rows', 10)

CURE = ["27DCU28I17"]
START_DATE_3years = dt.datetime(2016,1,1)
START_DATE_2years = dt.datetime(2017,1,1)
START_DATE_1years = dt.datetime(2018,1,1)
END_DTAE = dt.datetime(2018,12,31)
FILL_DATE = dt.datetime(2100,7,1)
DIVINE_LISTE = [ "IMMORTELLE DIVINE"]
PRECIOUS_LISTE = ["IMMORTELLE PRECIOUS"]
SUBLINE_LISTE= DIVINE_LISTE + PRECIOUS_LISTE




def main () :
    # Filter by country and target
    raw = load_clean(config_function.get_config("path_raw"), config_function.get_config("Columns_names"))

    # Get first target product date
    divine_cure_clients_raw = function.filtre_clients(raw, CURE, "uniquevipid", START_DATE_3years, END_DTAE)
    divine_cure_clients = function.first_target_product_purchased_date(divine_cure_clients_raw, "product_code", CURE,"uniquevipid", START_DATE_2years, END_DTAE)

    # KPI : number of VIP 1+ purchase
    function.purchase_at_least_one_time(divine_cure_clients, CURE, START_DATE_2years, END_DTAE)

    # KPI : Number of new clients
    number = function.number_new_clients(divine_cure_clients, CURE, START_DATE_2years, END_DTAE)

    # KPI : Number of retained clients :
    Retained_clients = function.number_retained_clients(divine_cure_clients, CURE, "uniquevipid")

    # KPI : Customers who purchase target product also purchase XXX :
    columns = {"first_target_product_purchase_date": "first_cure_purchase_date"}
    function.share_product_clients(Retained_clients, "sub_line", DIVINE_LISTE, "uniquevipid", columns).groupby("country").uniquevipid.nunique()

    # KPI :  Customers who purchase target product mais non purchase XXX :
    new_df = function.customer_non_purchase_target_product(Retained_clients, "sub_line", SUBLINE_LISTE, "uniquevipid",columns)

    # KPI :  Top products/sublines/segments that customers purchase :
    function.top_product(new_df, "segment", "JAPAN").head(10)


if __name__ == "__main__":
    main()

