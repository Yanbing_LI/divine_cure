# Divine Cure request analysis

## 1. Requirements

### 1.1. Packages
You may find all required python packages in the file /requirements.txt.

To install the packages in a new virtual venv (the recommended way), you should create a new venv with Anaconda:

        $ conda activate Data_Analysis
        $ pip install -r requirements.txt
