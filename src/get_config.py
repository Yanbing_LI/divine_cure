config = None
import json

def get_config(data_name=None):
    # Not a python expert enough to manage resources folders :/
    global config
    if config is None:
        with open('config.json') as config_file:
            config = json.load(config_file)
    if data_name is not None:
        return config[data_name]
    else:
        return config