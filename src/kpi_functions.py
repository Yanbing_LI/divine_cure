import pandas as pd
import datetime as dt
from datetime import timedelta

FILL_DATE = dt.datetime(2100,7,1)

# This function allows you to get a transaction dataset of customers who purchased at last one time target product,
# you get all his historical transactions (any purchase) over the periode
def filtre_clients(df,product_list,column_group, start_date, end_date) :
    df_copy = df[(df.date>=start_date)&(df.date<=end_date)].copy()
    df_copy["is_product"] = df_copy.product_code.isin(product_list)*1
    df_copy["temp"] = df_copy.groupby(column_group)["is_product"].transform(sum)
    df_copy=df_copy[df_copy.temp != 0].drop(["is_product","temp"],axis=1)
    return  df_copy



# This function allows you to get a new columns "first_target_product_purchase_date" in your dataset which present the first purchase date of a target product over the periode,
# if this colums = "2100-01-01"
def first_target_product_purchased_date (df,product_column,product_list,column_group,start_date, end_date) :
    df_copy = df.copy()
    df_filter = df_copy[
        (df_copy[product_column].isin(product_list))
        &(df_copy.date>=start_date)
        &(df_copy.date<=end_date)]
    df_filter["first_target_product_purchase_date"] = df_filter.groupby(column_group)["date"].transform(min)
    res = pd.concat([df_copy, df_filter["first_target_product_purchase_date"]], axis=1)
    res["first_target_product_purchase_date"]= res["first_target_product_purchase_date"].fillna(FILL_DATE)
    res["first_target_product_purchase_date"]= res.groupby(column_group)["first_target_product_purchase_date"].transform(min)
    return res


# KPI : number of VIP 1+ purchase by country
def purchase_at_least_one_time (df,product_list,start_date, end_date):
    return (df[df["product_code"].isin(product_list) &(df.date>=start_date) &(df.date<=end_date)].groupby("country").uniquevipid.nunique())


# DataSet new customers (new to brand and purchase target product in their 1st ticket)
def number_new_clients (df,product_list,column_group,start_date, end_date):
    df_copy = df[(df.date>=start_date)&(df.date<=end_date)].copy()
    df_copy ["temp"] = ((df_copy.first_purchase_date == df_copy.date)&(df_copy.product_code.isin(product_list)))*1
    df_copy["new"] = df_copy.groupby(column_group)["temp"].transform(sum)
    df_copy=df_copy[df_copy.new != 0].drop(["temp","new"],axis=1)
    #return df_copy.groupby("country").uniquevipid.nunique())
    return df_copy

# DataSet new customers repurchase after their 1 st ticket for 12 months (any purchase but excl 1st ticket)
def non_first_ticket (df) :
    df_copy = df.copy()
    df_copy = df_copy[(df_copy.date > df_copy.first_purchase_date) & ((df_copy.date <= df_copy.first_purchase_date + timedelta(days=365)))]
    return df_copy


# DataSet Retained CLients : Customers who purchase in l'Occitane over last 12 month before their first purchase target product date
    # Step1 :  find the previous purchased date before first target product purchased date
    # Step 2 : calculate the number of clients whoses first target product purchased date - previous purchased date < 365
    # Step 3 : Move new customers

def number_retained_clients (df,product_list,column_group):
    df_copy = df.copy()
    df_copy ["temp"] = df_copy[((df_copy.date >= df_copy.first_target_product_purchase_date - timedelta(days=365))
                                          &(df_copy.date < df_copy.first_target_product_purchase_date)
                                          &(df_copy.first_purchase_date != df_copy.first_target_product_purchase_date ))]["date"]
    df_copy["previous_purchase_date"] = df_copy.groupby(column_group)["temp"].transform(max)
    df_copy=df_copy[df_copy.previous_purchase_date.notna()].drop(["temp"],axis=1)
    return df_copy



# This function will give us the number of clients who purchased target product also purchased other special product before
def share_product_clients (df,product_column,product_list,column_group,columns_names) :
    df_copy = df.copy()
    df_copy = df_copy.rename(columns=columns_names)
    df_copy = df_copy[(df_copy.date< df_copy.first_cure_purchase_date)
                      & (df_copy.date >= df_copy.first_cure_purchase_date- timedelta(days=365))]
    df_copy["is_product"] = df_copy[product_column].isin(product_list)*1
    df_copy["temp"] = df_copy.groupby(column_group)["is_product"].transform(sum)
    df_copy=df_copy[df_copy.temp != 0].drop(["is_product","temp"],axis=1)
    return df_copy



# This function will give us a dataset that customer never purchase several products during our analysis periode
def customer_non_purchase_target_product (df,product_column,product_list,column_group,columns_names) :
    df_copy = df.copy()
    df_copy = df_copy.rename(columns=columns_names)
    df_copy = df_copy[(df_copy.date< df_copy.first_cure_purchase_date)
                      & (df_copy.date >= df_copy.first_cure_purchase_date- timedelta(days=365))]
    df_copy["is_product"] = df_copy[product_column].isin(product_list)*1
    df_copy["temp"] = df_copy.groupby(column_group)["is_product"].transform(sum)
    df_copy=df_copy[df_copy.temp == 0].drop(["is_product","temp"],axis=1)
    return df_copy


# Top segments and top products by country
def top_product (df, top_category, country) :
    df_copy = df[(df.country == country) ].copy()
    Total = df_copy.uniquevipid.nunique()
    df_copy = df_copy.groupby(top_category).uniquevipid.nunique().sort_values(ascending=False).to_frame()
    df_copy["max_clients"] = df_copy.uniquevipid.max()
    df_copy["ratio"] = df_copy.uniquevipid / Total
    df_copy = df_copy.drop("max_clients", axis=1)
    return  df_copy


# Avg ticket amount
def avg_ticket_amount (df) :
    df_copy = df.copy()
    df_copy.net_sales = df_copy.net_sales.astype(int) 
    ee = df_copy.groupby(["transaction_number","uniquevipid","country"])["net_sales"].agg(sum)
    return ee.groupby("country").mean()


# Frequency of purchasing target product
def frequency_target_product (df, product_list) :
    df_copy = df.copy()
    df_copy["is_product"] = df_copy.product_code.isin(product_list)*1
    df_copy = df_copy[df_copy.is_product == 1]
    df_copy_without_duplicates = df_copy[["uniquevipid","country","date","is_product"]].drop_duplicates()
    df_copy_without_duplicates["fre"] = df_copy_without_duplicates.groupby("uniquevipid")["is_product"].transform(sum)
    return df_copy_without_duplicates.groupby("country")["fre"].value_counts()

