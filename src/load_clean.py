import pandas as pd

# Transform date columns into datetime format
def transform_date (df,column) :
    df[column] = pd.to_datetime(df[column], format="%Y-%m-%d")


# load dataset, rename it and clean your date columns
def load_clean(df,Columns_names):
    # read data
    #data = pd.read_csv(PATH, sep=";", encoding='cp1252')
    data = df
    # remane your columns
    data = data.rename(columns=Columns_names)

    # Function to Transform date, first_purchase_date into str form
    transform_date(data,"date")
    transform_date(data, "first_purchase_date")

    return data

# Transform date columns into datetime format
def transform_date_csv (df,column) :
    df[column] = pd.to_datetime(df[column], format="%d/%m/%Y")
    
def load_clean_csv(PATH,Columns_names):
    # read data
    data = pd.read_csv(PATH, sep=";", encoding='cp1252')
    # remane your columns
    data = data.rename(columns=Columns_names)

    # Function to Transform date, first_purchase_date into str form
    transform_date_csv(data,"date")
    transform_date_csv(data, "first_purchase_date")

    return data